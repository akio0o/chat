




exports.user = function(Schema, mongoose){

    var UserSchema = new Schema({
        username    : String, // 名字，可以没有.默认是游客
        password    : String, // 密码可以没有
        rooms       : [],     // 参加的聊天室
        createrRooms: [],     // 创建的聊天室和对话
        sessionid   : String,
        ip          : String,
        rsses       : []
    });

    GLOBAL.userdb = mongoose.model("user", UserSchema);

};