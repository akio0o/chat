// socket设计
/*
    socket单位： chatroom

    accept:

        socket.on("client"+roomid, function(data){
            switch(data.type){
                case "newFeed": addFeed();break;
                case ""
            }
        });


    send:

        socket.emit("server"+roomid,{});
*/











(function(){

    var socket = io.connect('http://localhost:8089'),
        userId = $("#userid").val(),
        username = $(".member.self").html(),
        roomid = "",
        inited = {};

    var newmessagetmpl = '\
        <section class="chatitem">\
            <img width="23" height="23" src="/images/header.jpg">\
            <section class="feeditem">\
                <span class="name"></span><br>\
                <div class="content"></div>\
            </section>\
        </section>';

    var feedstmpl = $(".feeds.tmpl").hide(),
        memberstmpl = $(".members.tmpl").hide();

    var clientRoom = {

        addFeed: function(data){

            var feeds = $(".feeds."+data.roomid);
            var html = $(newmessagetmpl);
            if(data.userId == userId) html.addClass("myself");
            html.find(".name").html(data.user);
            html.find(".content").html(data.content).end().appendTo(feeds);
            feeds.scrollTop(100000);
        },

        newFeed: function(data){
            clientRoom.addFeed(data);
        },

        initRoom: function(data){
            var feed = data.feeds;
            for(var i = 0; i < feed.length; i++)
                clientRoom.addFeed(feed[i]);
        },

        renderUsers: function(data){
            var html = "", 
                member = memberstmpl.clone(true);

            member.attr("rid", data.roomid).addClass(data.roomid).insertAfter(memberstmpl);

            data.users.forEach(function(user){
                if(user.id !== userId) 
                html += "<a href='#' class='member "+user.id+"'>"+user.name+"</a>";
            });

            $(".members").hide();
            $(html).appendTo(member.show());            
        },

        offline: function(data){
            if(data.id === userId ) return;
            $("."+data.id).remove();
        },

        online: function(data){
            if(data.id === userId || $(".members."+data.roomid+" ."+data.id).html()) return;
            $("<a href='#' class='member "+data.id+"'>"+data.name+"</a>").appendTo($(".members"));
        },

        rename: function(data){
            $(".members."+data.roomid+" ."+data.id).html(data.name);
        }

    };

    var rooms = $(".rooms");
    rooms.on("click", ".group", function(){
        var roomid = this.getAttribute("data-id");
        var feeds  = $(".feeds."+roomid);
        if(!feeds.html()){
            feeds = feedstmpl.clone(true);
            feeds.insertAfter(feedstmpl);
            feeds.addClass(roomid);
        }

        $(".feeds").hide();
        feeds.show().scrollTop(100000);;
        if(inited[roomid]) return false;
        var typename = "server" + roomid;

        socket.on("client"+roomid, function(data){
            if(!data.type) data.type = "initRoom";
            clientRoom[data.type].call(socket, data, typename);
        });

        socket.emit("connected", {userId: userId, name:username, roomid: roomid});
        socket.emit(typename, {roomid: roomid, type: "initRoom", userId: userId});

        inited[roomid] = true;
    });
    
    var box = $(".sendbox .box"),
        sendbtn = $(".sendbox .sendbtn");

    sendbtn.on("click", function(event){
        var data = {},
            roomid = $(".group.selected").attr("data-id");

        if(box.val().length == 0) return;
        socket.emit("server"+roomid,{userId: userId, content: box.val(),roomid:roomid, type:"newFeed"});
        box.val("");
    });

    box.on("keydown",function(event){
        if(event.keyCode == 13 && event.ctrlKey == false){
            sendbtn.trigger("click");
            return false;
        }
    });

    $(".rooms .group:first").trigger("click");

    $("body").on("click", ".self", function(){

        var value = this.innerHTML,
            user  = $(this);
            roomid = this.parentNode.getAttribute("rid");
        this.innerHTML  = "<input>";
        user.find("input").on("click", false).on("blur keydown",function(){
            var me = $(this);

            if((event.type === "keydown" && event.keyCode == 13 && event.ctrlKey == false) || event.type === "blur"){

                if(this.value === value || this.value.length > 10 || this.value.length ==0){
                    me.css("border", "1px solid red");
                } else{
                    user.html(me.val());
                    socket.emit("server"+roomid,{userId:userId, name: me.val(), type:"rename", roomid:roomid});
                    me.off().remove();
                }
            }

        }).val(value).select().focus();
    });

    $("body").on("click", ".joinroom", function(){

        var id   = this.getAttribute("data-id"),
            name = this.getAttribute("data-name"),
            div = rooms.find("[data-id='"+id+"']");
        if(!div.html()){
            div = document.createElement("div");
            div.setAttribute("data-id",id);
            div.innerHTML = name;
            div.className = "group";
            rooms.append(div);
        }
        $(div).trigger("click");
    });

    if(localStorage.user != "inited"){
        document.cookie += "user="+userId+";expires=Sat, 04 May 2014 16:00:00 GMT";
        localStorage.user = "inited";
    }
})();
