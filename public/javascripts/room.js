(function(){

    var rooms = $(".rooms");

    rooms.on("click", ".group", function(){
        var s = $(this);
        if(!s.hasClass("selected")){
            rooms.find(".selected").removeClass("selected");
            s.addClass("selected");
        }
        return false;
    });

})();