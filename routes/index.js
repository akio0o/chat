
/*
 * GET home page.
 */

exports.index = function(req, res){

    var userdao = require("../core/UserDAO.js").UserDAO;

    if(req.cookies.user) req.session.user = req.cookies.user;

    if(!req.session.user){
        userdao.initUser(Date.now(), "游客"+(Date.now()+"").slice(-6), req.ip, function(err, user){
            req.session.user = user.id;
            console.log("----------------------",user)

            res.render('room', { title: "chat center", user: user});
        });
    } else{
        userdao.model.findOne({"_id": req.session.user}).exec(function(err, user){
                        console.log("----------------------",user)

            res.render('room', { title: "chat center", user: user});
        });
    }


    // var roomdao = require("../core/RoomDAO.js").RoomDAO;
    // roomdao.model.find({name:"name"}).exec(function(err,rooms){
    //     res.render('room', { title: "chat center", rooms:rooms});
    // });
};



exports.mobile = function(req, res){
    var userdao = require("../core/UserDAO").UserDAO;
    // if(req.cookies.user) req.session.user = req.cookies.user;
    if(!req.session.user){
        userdao.initUser(Date.now(), (Date.now()+"").slice(-6), req.ip, function(err, user){
            req.session.user = user.id;
            res.render('mobile', { title: "chat center", items: []});
        });
    } else{
        userdao.model.findOne({"_id": req.session.user}).exec(function(err, user){
            res.render('mobile', { title: "chat center", items: user.rsses});
        });
    }
};

exports.addrss = function(req, res){

    var url = req.query.link;
    var parser = require('xml2js').parseString;
    var http = require('http-get');
    var rssdao = require("../core/RSSDAO").RSSDAO;
    var userdao = require("../core/UserDAO").UserDAO;
    console.log(url)
    userdao.model.findOne({"_id": req.session.user}).exec(function(err, user){
        if(err) return res.json({code:"1",msg:"未登录"});
        http.get({url:url}, function(err, xml){
            if(err) return res.json({code:"1",msg:"无效的rss源"});
            parser(xml.buffer, function(err, json){
                if(err) return res.json({code:"1",msg:"RSS源解析错误"});
                var data = json.rss.channel[0],
                    rss  = {};
                rss.title          = data.title;
                rss.lastBuildDate  = data.lastBuildDate||data.pubDate;
                rss.description    = data.description;
                rss.managingEditor = data.managingEditor;
                rss.item           = JSON.stringify(data.item);
                rss.url = url;

                rssdao.addRSS(rss, function(err,r){});
                user.rsses.push({
                    link : url,
                    title: rss.title,
                    description: data.description.length < 10 ? rss.title : data.description
                });

                user.save(function(err, u){});
            });
        });
    });
};

exports.renderRss = function(req, res){
    var link = req.query.link;
    res.render('myrss', { title: "items" , link:link});
};

exports.rss = function(req, res){
    var link = req.query.link;
    console.log(link+"...........")
    GLOBAL.rssdb.findOne({url:link}).exec(function(err, Rss){
        if(err) return;
        res.json({ title: "chat center", items:JSON.parse(Rss.item)});
    });
};

exports.user = function(req, res){
    var userdao = require("../core/UserDAO").UserDAO;
    userdao.model.find({"_id": req.session.user}).exec(function(err, user){
        if(err) return res.json({code:"1",msg:"未登录"});
        res.render('mobile', { title: "All Rss", items:user.rsses});
    });
};
