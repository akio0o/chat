
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'tobo!'}));
  // app.use(express.session({secret:"akiasdasdasdasdasd"}));

  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));

});

app.configure('development', function(){
  // app.use(express.errorHandler());

});

app.get('/', routes.index);
app.get('/mobile', routes.mobile);
// app.get('/reg',register.register);
// app.get('/login',register.login);
// app.post('/user',register.user);
// app.get('/users', user.list);
// app.get('/chatroom', chatroom.chatroom);
// app.get('/room/:id', chatroom.enter);
// app.post('/createchatroom', chatroom.create);
// app.get('/center', chatroom.center);
// app.get('/message', chatroom.message);
// app.post('/message', chatroom.message);

app.post('/room',routes.index);
app.get('/addrss',routes.addrss);
app.get('/rss',routes.renderRss);
app.post('/rss',routes.rss);

http.createServer(app).listen(3000, function(){
  console.log("Express server listening on port " + app.get('port'));
});

// require("fs").readFile(__dirname+"/data/root11.json",function(err, conf){
//   console.log(conf)
//     // console.log(JSON.parse(conf.toString()));
// });

// var a = require("./controller/UserDAO").UserDAO;

// var b = require("./controller/UserDAO").UserDAO;

// setTimeout(function(){

//   a.addUser("asd","asdasd",{});

//   console.log(a.data, b.data); 

// }, 5000);
require("./db/initDB");
require("./common/times");
require('./common/chatServer');


var dao = require("./core/RoomDAO.js").RoomDAO;
//dao.createRoom("tmpchat", "Admin", false);
dao.model.find().exec(function(err,rooms){
    // dao.addFeedForRoom("515701daaa70b00e14000001","wwww");
    // dao.model.find({"_id":"515701daaa70b00e14000001"}).exec(function(err,rooms){
        // console.log(rooms);
    // });
});














