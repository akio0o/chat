
var Room = GLOBAL.roomdb;

exports.RoomDAO = {

    cache: {},

    model: Room,

    createRoom: function(name, username, isprivate, password, cb){

        var room = new Room();

        room.name = name;
        room.creater = username || "admin";
        room.isprivate = isprivate;
        room.password  = isprivate ? password :"";
        room.feeds = [];
        room.time  = GLOBAL.getServerTime();
        room.level = 20000; 
        room.topic = "";

        room.save(cb);
    },

    findByName: function(name, cb){
        Room.findByName(name,cb);
    },

    addFeedForRoom: function(roomid, feed){
        if(this.cache[roomid] === undefined){
            this.cache[roomid] = [];
        }
        this.cache[roomid].push(feed);
        if(this.cache[roomid].length === 100) this.update(roomid);
    },

    update: function(roomid){
        var me = this;
        Room.findOne({"_id":roomid}, function(err, room){
            var newFeeds = me.cache[roomid].concat(room.feeds);
            room.feeds = newFeeds;
            room.save();
        });
    },

    updateAll: function(){
        for(roomid in this.cache){
            this.update(roomid);
        }
    }

};











