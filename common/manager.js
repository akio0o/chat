
/*
    
    缓存所有的 聊天记录和聊天室的人
*/

var Rooms = {};

var roomdao = require("../core/RoomDAO").RoomDAO;

Rooms.createRoom = function(){
    roomdao.createRoom.apply(roomdao, arguments);
};

Rooms.initRoom = function(roomid, cb){
    roomdao.model.findOne({"_id":roomid}).exec(function(err, room){
        cb.apply(null, arguments);
        Rooms[roomid] = room;
    });
};

Rooms.getRoomInfo = function(roomid, cb){
    if(Rooms[roomid] == undefined){
        Rooms.initRoom(roomid, cb);
    } else {
        cb.call(null, null, Rooms[roomid]);
    }
};

Rooms.addFeed = function(roomid, feed){
    Rooms[roomid].feeds.push(feed);
    roomdao.addFeedForRoom(roomid, feed);
};

Rooms.getFeed = function(roomid){
    return Rooms[roomid].feeds;
};

Rooms.save = function(){
    roomdao.updateAll();
};


Rooms.userList = {};


Rooms.addUser = function(roomid, userid, username){
    if(this.userList[roomid] === undefined){
        this.userList[roomid] = {};
    }
    this.userList[roomid][userid] = username;
    this.save();
};

Rooms.removeUser = function(roomid, userid){
    if(this.userList[roomid] === undefined){
        this.userList[roomid] = {};
    }
    this.userList[roomid][userid] = null;
};

Rooms.rename = function(roomid, userid, name){
    if(this.userList[roomid] === undefined){
        this.userList[roomid] = {};
    }
    this.userList[roomid][userid] = name;
};

Rooms.getName = function(roomid, userid){
    return this.userList[roomid][userid];
};

Rooms.getUserList = function(roomid){
    
    var List = [];

    for(var userid in this.userList[roomid]){
        if(this.userList[roomid][userid] !== null)
            List.push({id: userid, name: this.userList[roomid][userid]});
    }

    return List;
};


exports.Rooms = Rooms;






