

exports.room = function(Schema, mongoose){ 

  var RoomSchema = new Schema({
    name     : String,
    topic    : String,
    isprivate: Boolean,
    password : String,
    creater  : String,
    time     : String,
    feeds    : [],
    level    : Number
  });


  RoomSchema.statics.findByName = function(name,cb){
    this.find({ name: new RegExp(name,'i') }, cb);
  };

  GLOBAL.roomdb = mongoose.model("Room", RoomSchema);
};


