
var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , xss = require("xss")
  , Chat = require("./chat")
  , MK = require( "markdown" )
  , MessageCenter = require("../data/message").MessagesCenter;

server.listen(8089);

var messageLog = new MessageCenter();

var action = new Chat.Chat(),
    userdao = require("../controller/UserDAO").UserDAO,
    sessiontimes = {};

io.sockets.on('connection', function (socket) {
   console.log("connection"); 

    var userList = action.userList,
        userId;

    socket.on("connected", function(data){

        userId = data.userId;
        var chatroomid = data.chatroomid;

        action.addUser(userId, data.name, data.chatroomid);

        var userlist = userList[data.chatroomid];

        socket.broadcast.emit("updateUserList"+chatroomid, userlist);
        socket.emit("updateUserList"+chatroomid, userlist);
        socket.emit("myname"+chatroomid, {name: data.name, userId: userId});


        messageLog.getMessages(data.chatroomid, function(messages){
            socket.emit("rendermessages"+chatroomid, messages);
        });

        if(sessiontimes[chatroomid] === undefined) sessiontimes[chatroomid]= {};
        if(sessiontimes[chatroomid][userId] === undefined) sessiontimes[chatroomid][userId] =0;
        sessiontimes[chatroomid][userId] ++;

        socket.on('message'+chatroomid, function (data) {
        
            data.time = action.getTime();
            data.content = MK.markdown.toHTML(data.content);
            data.user = userlist[userId];
            data.content = xss(data.content);

            messageLog.addMessage(data.chatroomid, data);

            socket.emit('newmessage'+chatroomid, data);
            socket.broadcast.emit("newmessage"+chatroomid,data);

        });

        socket.on('updatename'+chatroomid, function (data) {
            var result = action.rename(data.userId, data.name, data.chatroomid);

            if(result!=="error"){
                app.cache[data.userId] = data.name;
                return socket.broadcast.emit("updateUserList"+chatroomid,userlist);
            }
            socket.emit("errorname"+chatroomid,data);
        });

        socket.on('disconnect', function () {

            sessiontimes[chatroomid][userId]--;
            if(sessiontimes[chatroomid][userId] === 0){
                action.removeUser(userId,chatroomid);
                socket.broadcast.emit("updateUserList"+chatroomid,userList);
            }

            // socket.broadcast.emit("isonline"+chatroomid,{},function(data){
            //     console.log("ww")
            // });
            // timerid = setTimeout(function(){
            //     action.removeUser(userId,chatroomid);
            //     socket.broadcast.emit("updateUserList"+chatroomid,userList);
            // },1000);
        });

        var onlineuser = [];

        // setInterval(function(){
        //     onlineuser = [];
        //     socket.emit("isonline"+chatroomid);

        //     for( id in userlist){
        //         if( onlineuser.indexOf(id)  == -1)
        //             userlist[id] = "";
        //     }
        //     socket.broadcast.emit("updateUserList"+chatroomid,userList);
        // },3000);

        // socket.on('online'+chatroomid, function (data) {
        //     // console.log(data.code+"data.code")
        //     // if(data.code == 1) clearTimeout(timerid);
        //     onlineuser.push(data.userid);
        // });
    });


    

    
});
