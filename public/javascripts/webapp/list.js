


$(document).ready(function(){

    if(localStorage[location.search] && false) {
        renderList(JSON.parse(localStorage[location.search]));
    } else {
        ajax();
    }

    



    function set(link, json){
        localStorage[link] = JSON.stringify(json);
    }
    function get(link){
        return JSON.parse(localStorage[link]);
    }

    function renderList(data){
        var ul = $("ul");
        data.items.forEach(function(item){

            var description = slize(item);
            item.description = description;
            var str = description.slice(0,100).trim()
                .replace(/<br(?:[^.]*)(?:\/)>/mg,"");
            $('<li data-url="'+item.link+'"><h3>'+item.title+'</h3><section>'+str+'</section></li>').appendTo(ul);
            set(item.link+"",JSON.stringify(item));
        });
    }

    function ajax(){
        $.post("/rss"+location.search, function(data){
            renderList(data)
            set(location.search, data);
        });
    }

    function slize(item){
        var des = item.description,
            description = "";
        if(Array.isArray(des)){
            des.forEach(function(item){
                if(typeof item === "string"){
                    description += item;
                }
                if(typeof item === "object"){
                    for(var name in item){
                        if(typeof item[name] === "object")
                            for(var k in item[name]) description += item[name][k];
                        else description += item[name];
                    }
                }
            });
        }else{
            description = des;
        }
        return description.replace(/[ ]{8}/mg, "");
    }

    function renderArticle(data){
        data = JSON.parse(data),
        $(".article h3").html(data.title);
        $(".article section").html(data.description);
        $("ul").hide();
        $(".article").show();
        window.scrollTo(0,1);
    }

    $("ul").on("click","li", function(){
        var link = this.dataset.url;
        var data = JSON.parse(localStorage[link]);
        renderArticle(data);
        // history.pushState({link:link},"detail","detail")
    });

    $(".article").on("click", function(){
        $(".article").hide();
        $("ul").show();
    });

    if(window.navigator.standalone) alert("installed");
});