


var syntax = {};

var roomdao = require("../core/RoomDAO").RoomDAO;

syntax.createRoom = function(str){

    if (str.toLowerCase().indexOf("@createroom") == 0 ){
        var name = str.split(" ")[1];
        if(name.replace(/[^\x00-\xff]/g, "--").length > 20){
            return { "error" : "room名称过长"};
        }
        return {"success": "createroom",name: name};
    }

    return this.shareRoom.call(this,str);
};


syntax.shareRoom = function(str){

    if (str.slice(0, 10) === "@shareRoom") {
        
        var name = str.split(" ")[1];
        if(name.replace(/[^\x00-\xff]/g, "--").length > 20){
            return { "error" : "room名称过长"};
        }
        return {"success": "shareRoom"};
    }

    return str;
};

























exports.syntax = syntax;







