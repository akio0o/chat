

var mongoose = require('mongoose');

var db = mongoose.connection;
mongoose.connect('mongodb://localhost/test');

Schema = mongoose.Schema;

require("./room").room(Schema, mongoose);
require("./user").user(Schema, mongoose);
require("./RSS").RSS(Schema, mongoose);
