




exports.RSS = function(Schema, mongoose){

    var RSSSchema = new Schema({
        title          : String, 
        description    : String, 
        lastBuildDate  : String,     
        url            : String,     
        managingEditor : String,
        item           : []
    });

    GLOBAL.rssdb = mongoose.model("RSS", RSSSchema);

};