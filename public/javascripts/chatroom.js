(function(){
    var tmpl = function(tmpl, data){
        return tmpl.replace(/\{(\w+)\}/g, function(str, key){
            return data[key];
        });
    };

    var contentTmpl = '<div class="content">{content}</div>',
        messageTmpl = 
        '<div class="message">' +
            '<div class="header">' +
                '<a href="#" class="user">{user}</a>'+
                '<span class="time">{time}</span>'+
            '</div>'+
            contentTmpl+
        "</div>";


    var message = $(".sendtext"),
        sendbtn = $(".sendbtn"),
        lastMessage = {},
        messagebox = $(".messagebox"),
        chatroomid = $("#chatroomid").html();

    var uid = $(".userlist .item:first").attr("data-uid");

    var sendtype = $(".sendtype");
    
    var socket = io.connect('http://42.96.174.146:8089');

    var userTmpl = '<a class="item">{userId}</a>',
        userlist = $(".userlist");

    socket.on("updateUserList"+chatroomid,function(userList){

        var html = "",
            user = 0;

        if(userList[chatroomid]) userList = userList[chatroomid];
        
        for (var name in userList) {
            if(userList[name] == "") continue;
            html += '<a class="item" data-uid="'+name+'">'+userList[name]+'</a>';
            user++;
            // if(uid == name) $(".userlist .item:first").html(userList[name]);
        }
        $(".userlist .item:not(:first)").remove();
        $(html).appendTo(userlist);
        userlist.find(".title:last").html('在线用户'+user+'位');
    });


    
    socket.emit("connected",{userId: $("#userId").html(), name: $("#userName").html(), chatroomid: chatroomid});

    socket.on("myname"+chatroomid, function(data){
        userlist.find(".creater").html(data.name).attr("data-uid", data.userId);
        document.cookie = "username="+ data.name+";expires=Sat, 04 May 2013 16:00:00 GMT";
    });

    socket.on("errorname"+chatroomid, function(data){
        // userlist.find(".creater").html(data.oldname);
        userlist.find("[data-uid="+data.userId+"]").html(data.oldname);
        // console.log(userlist.find(".creater"));
        alert("此用户名已经存在");
    });

    socket.on('newmessage'+chatroomid, function (data) {
        var user = data.user,
            html = "";

        var carr = data.time.split(":"),
            current = +carr[carr.length-1],
            larr = lastMessage.time == undefined ? [-10] : lastMessage.time.split(":"),
            last = +larr[larr.length-1];

        if(user == lastMessage.user && (current - last) < 4){
            html = tmpl(contentTmpl, data);
            $(".message:last").append(html);
        } else{
            html = tmpl(messageTmpl, data);
            messagebox.append(html);
            lastMessage = data;
        }
        messagebox.scrollTop(1000000);
    });

    socket.on("rendermessages"+chatroomid, function(messages){
        var html = "";
        $.each(messages,function(i, message){
            html += tmpl(messageTmpl, message);
        });
        $(".messagebox").html(html);
        messagebox.scrollTop(1000000);
    });
    
    socket.on("isonline"+chatroomid, function(){
        socket.emit("online"+chatroomid, {userid: uid});
    });

    sendbtn.click(function(event){
        socket.emit('message'+chatroomid,
            {content:message.val(), sendtype:sendtype.find(".btn-primary").attr("data-type"),chatroomid:chatroomid}
        );message.val("").focus();
        return false;
    });

    userlist.on("click",".item:first",function(){
        var me = $(this),
            editor = $("<input type='text' class='itemeditor'>");
        editor.insertAfter(me);
        editor.val(me.html()).select().focus();

        var save = function(){
            var newname = editor.val(),
                uid = me.attr("data-uid"),
                oldname = me.html();

            if(newname != oldname){
                socket.emit("updatename"+chatroomid,{userId:uid,name:newname,oldname:oldname,chatroomid:chatroomid});
                userlist.find("[data-uid="+uid+"]").html(newname);
                document.cookie = "username="+ newname+";expires=Sat, 04 May 2013 16:00:00 GMT";
            }
            me.html(newname).show();
            editor.remove();
        };

        editor.blur(save);
        editor.keydown(function(event){
            if(event.keyCode == 13) save(); 
        });
        me.hide();
    });

    message.keydown(function(e){
        if(e.keyCode == 13 && e.shiftKey != true){
            sendbtn.trigger("click");
            return false;
        }
    });
    var target = 0;
    message.focus(function(){
        //if(target == 200) 
	return;
        var timerid =setInterval(function(){
            target += 20;
            target = target > 200 ? (clearTimeout(timerid),200) : target;
            message[0].style.height = target + "px";
        },10);

    });

    message.blur(function(){
        return;
	//if(target == 0) return;
        var timerid =setInterval(function(){
            target -= 20;
            target = target < 60 ? (clearTimeout(timerid),40) : target;
            message[0].style.height = target + "px";
        },10);

    });
    
    sendtype.on("click", ".format", function(event){
        $(this).addClass("btn-primary").siblings(".format").removeClass("btn-primary");
    });
    
})();
