





GLOBAL.getServerTime = (function(){

    // 避免 1秒内执行过多次
    
    var currentTime = "";

    return function(){

        var date = new Date();

        if(currentTime) return currentTime;
        setTimeout(function(){
            currentTime = false;
        },1000);
        currentTime = date.getFullYear() + "." + (date.getMonth()+1) + "." + date.getDate() + ".  " + date.getHours() + ":" +date.getMinutes() + ":" + date.getSeconds();
    
        return currentTime;
    }; 
})();







