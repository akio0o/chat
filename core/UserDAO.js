
var User = GLOBAL.userdb;

exports.UserDAO = {

    model: User,

    initUser: function(sessionid, username, ip, cb){

        var user = new User();

        user.sessionid = sessionid;
        user.username = username || "游客";
        user.password = "";
        user.ip       = ip;
        //515a56615305020a7c000001
        //5157e9d08820329c19000001
        user.rooms.push({id:"5157e9d08820329c19000001",name:"tmpchat"});
        user.save(cb);
    },


    addRoom: function(userid, roomid, roomname, isCreater){
        User.findOne({"_id":userid}).exec(function(err, user){
            var room = {id:roomid, name: roomname},
                isexist = false;
            user.rooms.forEach(function(item){
                if(item.id == roomid){
                    isexist = true;
                    return false;
                }
            });
            if(isexist) return;
            user.rooms.push(room);
            isCreater && user.createrRooms.push(room);
            user.save();
        });
    },

    createRoom: function(userid, roomid, roomname){
        this.addRoom(userid, roomid, roomname, true);
    },

    joinRoom: function(userid, roomid, roomname){
        this.addRoom(userid, roomid, roomname, false);
    },

    addRSS: function(userid, rss){
        User.findOne({"_id":userid}).exec(function(err, user){
            user.rsses.push(rss);
            user.save();
        });
    }

};























