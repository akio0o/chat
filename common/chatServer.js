
var app    = require('express')(),
    server = require('http').createServer(app),
    io     = require('socket.io').listen(server),
    xss    = require("xss"),
    MK     = require( "markdown" ),
    Rooms  = require("./manager").Rooms,
    syntax = require("./syntax").syntax,
    roomdao = require("../core/RoomDAO").RoomDAO,
    userdao = require("../core/UserDAO").UserDAO;

// 所有服务器的socket逻辑。#2013,4,1 最好的解决方案
var serverRoom = {

    newFeed: function(data, typename){
        var roomid = data.roomid;
        data.time = GLOBAL.getServerTime();
        data.user = Rooms.getName(data.roomid, data.userId);

        var r = syntax.createRoom(data.content),
            socket = this;

        if( r.success == "createroom"){
            roomdao.createRoom(r.name, data.user, false, "", function(err, room){
                data.content = "<p><code>"+data.user+"</code>创建了聊天室<code>"+room.name+"</code>(<a href='#' class='joinroom' data-id='"+
                    room.id+"' data-name="+room.name+">点击加入</a>)</p>";
                data.type = "newFeed";
                socket.emit(typename, data);
                socket.broadcast.emit(typename, data);
                Rooms.addFeed(data.roomid,data);
                userdao.createRoom(data.userId, data.roomid, room.name);
            });
        } else {
            data.content = MK.markdown.toHTML(data.content);
            data.content = xss(data.content);
            data.type = "newFeed";
            this.emit(typename, data);
            this.broadcast.emit(typename, data);
            Rooms.addFeed(data.roomid,data);
        }
    },

    initRoom: function(data, typename){
        var me   = this;
        Rooms.getRoomInfo(data.roomid, function(err, room){
            me.emit(typename, room);
            me.emit(typename, {users:Rooms.getUserList(data.roomid),roomid:data.roomid,type:"renderUsers"});
            userdao.createRoom(data.userId, data.roomid, room.name);
        });
    },

    offline: function(data, typename){
        Rooms.removeUser(data.roomid, data.userId);
        this.broadcast.emit(typename, {id: data.userId,type:"offline"});
    },

    online: function(data, typename){
        Rooms.addUser(data.roomid, data.userId, data.name);
        this.broadcast.emit(typename, {id: data.userId, name: data.name,type:"online"});
    },

    rename: function(data, typename){
        Rooms.rename(data.roomid, data.userId, data.name);
        this.broadcast.emit(typename, {id: data.userId, name: data.name,type:"rename", roomid: data.roomid});
    }

};

io.set('transports', ['websocket','flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']);

server.listen(8089);

io.sockets.on('connection', function (socket) {
    socket.on("connected", function(data){

        var typename = "client" + data.roomid;
        Rooms.addUser(data.roomid, data.userId, data.name);
        socket.on("server"+data.roomid, function(data){
            serverRoom[data.type].call(socket, data, typename);
        });
        socket.on("disconnect",function(){
            serverRoom.offline.call(socket, data, typename);
        });
        serverRoom.online.call(socket, data, typename);
    });

});
